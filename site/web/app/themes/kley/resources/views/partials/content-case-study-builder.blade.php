@while(have_rows('sections'))
  @php
    the_row();
    $layout = get_row_layout();
    $class = 'App\\Builder\\blockCaseStudy';
    $array = call_user_func( ['App\Builder\Config', $layout] );
    $block = new $class($array);
  @endphp

  @includeIf('partials.builder.' . $layout, ['block' => $block])

@endwhile
