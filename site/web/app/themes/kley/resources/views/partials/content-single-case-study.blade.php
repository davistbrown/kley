@if( have_rows( 'sections' ) )
    @include('partials.content-builder')
    @else
        @include('partials.page-header')
        @include('partials.content-page')
    @endif
@endwhile