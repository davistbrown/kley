<?php

namespace App;
use StoutLogic\AcfBuilder\FieldsBuilder;

$w50 = array(
    'width'=>'50%'
);
$w33 = array(
    'width'=>'33.333%'
);
$w66 = array(
    'width'=>'66.666%'
);


$case_study_builder = new FieldsBuilder('case_study_builder');

$case_study_builder
    ->setLocation('page_template', '!=', 'views/template-custom.blade.php')
        ->and('post_type', '==', 'case_study')
    ->setGroupConfig('hide_on_screen', [
        'the_content',
        'featured_image',
        "page_attributes",
    ]);

$case_study_builder
    ->addFlexibleContent('sections', ['button_label' => 'Add Section'])

        // Intro
        ->addLayout('intro')
            ->addWysiwyg("content")

        // Large Image
        ->addLayout("large_image")
            ->addSelect('width')
                ->addChoices(['full_width' => 'Full Width'], ['contained' => 'contained'])
            ->addImage("image")

        // Text Block
        ->addLayout("content_block")
            ->addSelect('background')
                ->addChoices(['white' => 'White'], ['light' => 'Light'])
            ->addWysiwyg("content")

        // Image Grid
        ->addLayout("image_grid")
            ->addSelect("columns")
                ->addChoices(['2_columns' => '2 Columns'], ['3_columns' => '3 Columns'])
            ->addRepeater("images")
                ->addImage("image")

        // Image Slider
        ->addLayout("image_slider")
            ->addRepeater("images")
                ->addImage("image")

        // Images with Text
        ->addLayout("images_with_text")
            ->addRepeater("images")
                ->addImage("image")
                ->addWysiwyg("content")

        // Scrolling Site
        ->addLayout("scrolling_site")
            ->addWysiwyg("content")
            ->addImage("desktop_site")
            ->addImage("mobile_site");

return $case_study_builder;