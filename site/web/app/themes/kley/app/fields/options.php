<?php

namespace App;
use StoutLogic\AcfBuilder\FieldsBuilder;

$w50 = array(
    'width'=>'50%'
);
$w33 = array(
    'width'=>'33.333%'
);
$w66 = array(
    'width'=>'66.666%'
);


$global_settings = new FieldsBuilder('global_settings');

$global_settings
    ->setLocation('options_page', '==', 'global-options');

$global_settings
    ->addImage('logo', ["wrapper"=>$w50])
    ->addWysiwyg('footer_statement', ['new_lines' => 'br'])
    ->addWysiwyg('address', ['new_lines' => 'br'])
    ->addWysiwyg('contact', ['new_lines' => 'br'])
    ->addText('copyright');

return $global_settings;