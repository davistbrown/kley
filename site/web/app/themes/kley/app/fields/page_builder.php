<?php

namespace App;
use StoutLogic\AcfBuilder\FieldsBuilder;

$w50 = array(
    'width'=>'50%'
);
$w33 = array(
    'width'=>'33.333%'
);
$w66 = array(
    'width'=>'66.666%'
);

$page_builder = new FieldsBuilder('page_builder');

$page_builder
    ->setLocation('page_template', '!=', 'views/template-custom.blade.php')
        ->and('post_type', '==', 'page')
    ->setGroupConfig('hide_on_screen', [
        'the_content',
        'featured_image',
        "page_attributes",
    ]);

$page_builder
    ->addFlexibleContent('sections', ['button_label' => 'Add Section'])

        // Intro
        ->addLayout('intro')
            ->addWysiwyg("content")
            ->addImage("image")

        // Work
        ->addLayout('work')
            ->addText("header")
            ->addLink("work_link")
            ->addRepeater("case_studies", ["layout" => "block"])
                ->addWysiwyg("content")
                ->addImage("primary_image")
                ->addImage("secondary_image")
                ->addLink("link")

        ->addLayout("work_filter")
            ->addLink("work_link")
            ->addRepeater("work")
                ->addWysiwyg("content")
                ->addImage("image")
                ->addLink("link")

        // Services
        ->addLayout('services')
            ->addWysiwyg("content")
            ->addLink("services_link")
            ->addRepeater("services")
                ->addWysiwyg("content")
                ->addLink("link")

        // Services Detailed
        ->addLayout('services_detailed')
            ->addRepeater("services")
                ->addWysiwyg("content")
                ->addRepeater("capabilities")
                    ->addText("label")
                ->addImage("image")
                ->addLink("link")

        // Partners
        ->addLayout('partners')
            ->addWysiwyg("content")
            ->addGallery("logos")

        // Team
        ->addLayout('team')
            ->addSelect('background')
                ->addChoices(['white' => 'White'], ['light' => 'Light'])
            ->addSelect("columns")
                ->addChoices(['3_columns' => '5 Columns'], ['5_columns' => '5 Columns'])
            ->addWysiwyg("content")
            ->addRepeater("people")
                ->addImage("image")
                ->addText("name")
                ->addText("title")
                ->addTextArea("location")

        // Quote
        ->addLayout('quote')
            ->addTextArea("quote")
            ->addText("author")

        // Image
        ->addLayout('image')
            ->addImage("image");

return $page_builder;