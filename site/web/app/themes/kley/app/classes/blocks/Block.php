<?php
namespace App\Builder;
use WP_Query;

class Block  {

    public $classes = [];
    public $styles = [];
    /* --------------------------------------------------------------- */
	public function __construct() {
        global $count;
        $this->position = intval( $count++ );
        $this->id = "block-".$this->position;
        $this->prefix = str_replace('_','-', get_row_layout());
        $this->add_classes(["block", 'b-'.$this->prefix]);

        foreach ( get_fields("sections", get_the_id()) as $prop ) {
            // $this->{$prop} = get_sub_field( $prop );
            var_dump($prop);
        }
	}
    /* --------------------------------------------------------------- */
    // Add block classes
    public function add_class( $class ) {
		$this->classes[] = $class;
    }
	public function add_classes( array $classes ) {
		foreach ( $classes as $class ) {
			$this->classes[] = $class;
		}
    }
    /* --------------------------------------------------------------- */
    // Check for block classes
    public function has_class(String $class = "") {
		return in_array($class, $this->classes);
	}
	public function has_classes(array $classes = []) {
		return count(array_intersect($classes, $this->classes)) == count($classes);
    }
    public function has_classes_or(array $classes = []) {
		return count(array_intersect($classes, $this->classes)) != 0;
	}
    /* --------------------------------------------------------------- */
	// Join styles, classes into string
	public function classes() {
		return join(" ", $this->classes);
	}
	public function styles() {
        $styles = [];
		foreach ( $this->styles as $prop => $value ) {
			$styles[] = $prop . ': ' . $value . ';';
        }
		return join( ' ', $styles );
    }
    /* --------------------------------------------------------------- */
    //  Make repeater field array into an object
    public function array_to_object($array) {
        if (is_array($array)) {
            $obj = (object)[];
            foreach($array as $k => $v) {
                if(strlen($k)) {
                    if(is_array($v)) {
                        $obj->{$k} = $this->array_to_object($v); //RECURSION
                    } else {
                        $obj->{$k} = $v;
                    }
                }
            }
            return $obj;
        } else {
            return false;
        }
    }
}